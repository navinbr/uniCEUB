---
title: Notas Acadêmicas
subtitle: Interação Humano-Computador
author: Navin Ruas
teacher: 
date: 27/02/2023
documentclass: report
output:
  pdf_document:
    toc: true
header-includes:
  - \usepackage{fancyhdr}
  - \pagestyle{fancy}
  - \fancyhf{}
  - \lhead{\textbf{Notas Acadêmicas}}
  - \rhead{\textbf{\leftmark}}
---

\renewcommand{\contentsname}{Sumário}
\tableofcontents*

\newpage

# Anotações de Sala

## Aula 00: Apresentação da Disciplina

- Atividades avaliativas gerais do professor 01: até 26/05 (25%)
- Prova 1: 24/04 (25%)
- Atividades avaliativas gerais do professor 02: até 01/07 (25%)
- Prova 2: 19/06 (25%)

UA: 04/06 **NÃO TEM MAIS PROVA DA UAs, SOMENTE CONTA COMO PRESENÇA**

## Aula 01: Introdução à Humano-Computador

### O QUE É IHC ?

Interação humano-computador (IHC, também conhecida como interação homem-computador) é o estudo da interação entre pessoas e computadores.

É uma matéria interdisciplinar que relaciona ciência da computação, artes, design, ergonomia, psicologia, sociologia, semiótica, linguística, e áreas afins.

### Introdução à IHC

IHC é a abreviação para **INTERAÇÃO HUMANO-COMPUTADOR**, que no inglês é encontrado sob a sigla *HCI – Human-Computer Interaction*.

IHC também pode ser interpretado como **INTERFACE HOMEM-COMPUTADR**, mas este equívoco de comparação ocorre devido ao histórico dos termos interface e interação.

Mesmo assim o termo IHC possui afinidade com as questões de ‘interface com o usuário’ (Preece, 1994).

#### O INÍCIO... INTERAÇÃO MANUAL

**Turing e sua máquina**

*Seu título de “pai da computação” só veio com a criação de sua “máquina-automática”, atualmente conhecida pormáquina de Turing.*

*A ideia do equipamento era bastante simples: o aparelho devia ser capaz de manipular símbolos em uma fita de acordo com uma série de regras para guardar informações. O conceito parece familiar?*

*Afinal, é assim que todos os computadores funcionam.*

O termo ‘interface’ foi inventado por volta de 1880, mas a palavra não teve muita repercussão até 1960, quando começou a ser utilizada pela indústria computacional.
O termo interface é absorvido e seu uso generalizado, designando o ponto de interação entre um computador e outra entidade, tais como impressoras ou operadores humanos.
Isso ocorre na década de 1970, quando pesquisadores da área computacional passam a se preocupar com estudos sobre a *‘Interface com o Usuário’* (*UI – User Interface*) também conhecida por ‘Interface Homem Máquina (*MMI – Man-Machine Interface*).

**Interface e Interação são coisas diferentes.**

##### Termos comumente usados:
**INTERAÇÃO**: Enfoque mais amplo com novos campos de estudo envolvendo a comunicação entre usuários e computadores ou outros tipos de produtos.
**INTERFACE**: Termo pioneiro que estabelece o conceito de ponto de interação entre um computador e outra entidade.

###### Interface
A interface é responsável por promover estímulos de interação para que o usuário obtenha respostas relacionadas às suas atividades.

A interface é tanto um meio para a interação usuário-sistema, quanto uma ferramenta que oferece os instrumentos para este processo comunicativo.

Desta forma “*a interface é um sistema de comunicação*” (de Souza, 1999).
Em um sistema computacional a interface com o usuário é o conjunto completo de aspectos que torna explícito o processo de interação e inclui de forma resumida os seguintes elementos:
- dispositivos de entrada e saída de dados;
- informação apresentada ao usuário ou enviada pelo usuário;
- retorno oferecido pelo sistema ao usuário;
- comportamento do sistema; e
- ações do usuário com respeito a todos estes aspectos.

Os componentes de interface possibilitam a comunicação entre usuário e equipamentos ou dispositivos (**entrada e saída de dados**).

Interação é a troca que ocorre entre usuários e equipamentos, a exemplo dos sistemas computacionais.

Isso acontece por meio de ações básicas e habituais, que são as tarefas de interação.

Em sistemas computacionais a configuração dos processos de interação com especificações personalizadas podem oferecer ao usuário experientes flexibilidade durante a interação.


#### A EVOLUÇÃO... INTERAÇÃO POR TECLADO

**MS-DOS -Microsoft DiskOperating System**

Dentro da História da computação é considerado por alguns como sendo o produto que decidiu o destino da então minúscula Microsoft. 
O *MS-DOS* foi sucedido por duas linhas de produtos: o *OS/2* e o Windows 3.11. 
O desenvolvimento destes sistemas operacionais, incluindo o o Windows NT, contribuíram signficativamente para a evolução da informática nas décadas de 80 e 90.

### Objetos de Estudo em IHC

- **A natureza da interação** envolve investigar o que ocorre enquantoas pessoas utilizam sistemas interativos em suas atividades.
- **O contexto de uso influência** a interação de pessoas com sistemas interativos, levando em conta a cultura, sociedade e organização e linguagem.
- As **características humanas** forma como as pessoas se comunicam e interagem, entre si e com outros artefatos, essas são as mesmas formas de interação quando lidam com um sistema computacional interativo.
- **A arquitetura de sistemas computacionais e interfaces** com usuário buscando construir sistemas que favoreçam a experiência de uso.

### IHC COMO ÁREA MULTIDISCIPLINAR

IHC se beneficia de conhecimentos e métodos de outras áreas fora da Computação para conhecer melhor os fenômenos envolvidos no uso de sistemas computacionais interativos.

IHC se beneficia de conhecimentos e métodos de outras áreas como:
- Psicologia
- Sociologia
- Antropologia
- Design
- Ergonomia
- Linguistica
- Semiótica

***Observação:** A responsabilidade de cuidar deIHC deve ser atribuída a uma equipe multidisciplinar. Dessa forma, profissionais com formações diferentes podem trabalhar em conjunto, concebendo e avaliando a interação de pessoas com sistemas computacionais.*

**Interação, como linguagem artificial, depende de design.**

#### Áreas correlatas a IHC na computação
- ESW Linguagens, BD, Web engineering;
- Interação Humano-Computador IHC CG Multimídia, hipermídia, realidade virtual;
- Aprendizado de máquina, visão computacional, robótica;

*Área mais próxima: Engenharia de Requisitos*

##### Se a relação com o usuário é importante, como as empresas a traduzem nos requisitos?
- “O sistema deverá ser amigável”
- “O sistema deverá ser fácil de aprender”
- “A interface deverá seguir o manual de estilo”
- “A interface deverá se adaptar às necessidades do usuário
- “A interface deverá ter design simples e intuitivo”
- “O estagiário vai cuidar da interface.

### Affordances

A definição de *Affordances* foi originalmente proposta pelo psicólogo James Gibson em 1977 para denotar a qualidade de qualquer objeto que permite ao indivíduo identificar suas funcionalidades através de seus atributos (forma, tamanho ou peso) de maneira intuitiva e sem explicações.

Não demorou muito para o termo ser utilizado nas interfaces digitais, pela disciplina de IHC (Interação Humano-Computador) e Design de Interação, afinal, quando desenhamos os elementos de uma aplicação, muitas vezes adicionamos sombra, relevo e outros efeitos visuais, geralmente baseados em objetos utilizados no mundo real.

#### Tipos de Affordance

*As affordances são divididas em 4 tipos, que podem facilitar muito o processo de desenho da interface.*
- **Explícita**: Quando tornamos um elemento óbvio e claro das suas funcionalidades, indicando exatamente o que precisa ser feito. 

O famoso “*slide to unlock*” do iOS, era isso. Além de mostrar um botão em formato de deslizante, tinha uma mensagem clara explicando o que devia fazer.

- **Padrão ou Convencional**: Sempre são baseadas em padrões antes vividos pelos usuários, ou pelo menos pela grande maioria deles.

Exemplo são os hiperlinks azuis que aparecem nas páginas web.

- **Oculta**: Uma Affordance oculta fica aparente somente quando uma certa condição é completada. 

Por exemplo a tela bloqueada do iOS no iPhone X, tirou a obviedade do “Slide to Unlock” para um minimalismo, onde de primeira o usuário não recebe nenhuma informação do que deve ser feito para o desbloqueio.

- **Metafórica**: Desde as primeiras interfaces gráficas, sempre samos referências do mundo real como metáforas em interfaces digitais.

Ícones e símbolos são um grande exemplo disso: o carrinho de compra, o envelope, o telefone, o vídeo, a câmera, a impressora, etc.

### BENEFÍCIOS DA IHC
- Aumentar a produtividade dos usuários, pois, se a interação for eficiente, os usuários podem receber apoio computacional para alcançar seus objetivos mais rapidamente;
- Reduzir o número e a gravidade dos erros cometidos pelos usuários, pois eles poderão prever as consequências de suas ações e compreender melhor as respostas do sistema e as oportunidades de interação;
- Reduzir o custo de treinamento, pois os usuários poderão aprender durante o próprio uso e terão melhores condições de se sentirem mais seguros e motivados para explorar o sistema;
- Reduzir o custo de suporte técnico, pois os usuários terão menos dificuldades para utilizar o sistema e, se cometerem algum erro, o próprio sistema oferecera apoio para se recuperarem dos erros cometidos;
- Aumentar as vendas e a fidelidade do cliente, pois os clientes satisfeitos recomendam o sistema a seus colegas e amigos e voltam a comprar nova versões.

## Aula 02: Introdução à Interação Humano-Computador - Complemento 

### Propostas

#### Gerais

- NBR ISO 9000:2005 – define princípios e vocabulário.
- NBR ISO 9001:2000 – define exigências para sistema de gerência de qualidade.
- NBR ISO 9004:2000 – apresenta linha diretivas para o melhoramento do desempenho da empresa.

#### Produto

- McCall (1977) , FURPS.
- Usabilidade (Nielsen).
- IHC.
- ISO/IEC 9126 – Qualidade do produto.
- ISO/IEC 25000:2005 (projeto SQUARE) - introdução geral.

#### Processo

- ISO/IEC 12207 – Processo de ciclo de vida do software.
- CMMI – Modelo de capacidade de maturação para software.
- ISO/IEC 15504 – Modelo de melhoramento e avaliação do Processo.
- MPS BR.

#### Serviço

- ISO 20000.
- ITIL.

### Interação Humano Computador

* Interação
  + Processo de comunicação entre pessoas e sistemas interativos
  + Assim, a interação envolve, além da interface, o ambiente físico onde o trabalho é desenvolvido.
* Interface
  + Parte de um sistema com a qual um usuário mantém contato ao utilizá-lo, tanto ativa quanto passivamente.
 + Engloba: 
    - Software (aplicativos, sistemas operacionais, etc.).
    - Hardware (dispositivos de entrada e saída, como por exemplo: teclados, mouse, tablets, monitores, impressoras e etc.).

#### Poluição Visual

![Poluição Visual Exemplo](imgs/PoluicaVisualExemplo.png)

#### Qualidade da Interção

A qualidade da interação determina se os usuários aceitam ou 
recusam um sistema.

Para o usuário, a interface é o sistema: 
- Se a interface não é boa, o sistema não é bom! 
- Se a interface é boa, o sistema é bom!

A qualidade da interação é crítica em certas atividades, como o 
controle de tráfego aéreo ou de usinas nucleares.

### Interação Humano Computador

Área de estudo importante no inicio da década de 80.

Também conhecida como Interface Homem-Máquina (IHM). (Em inglês: HCI Human-Computer Interaction).

“É uma disciplina relacionada ao projeto, implementação e avaliação de sistemas computacionais interativos para uso humano e ao estudo dos principais fenômenos que os cercam.” (ACM SIGCHI, 1992, p.6).

Área multidisciplinar, que envolve disciplinas como: 
- Ciência da Computação; 
- Psicologia Cognitiva; 
- Psicologia Social e Organizacional; 
- Ergonomia ou Fatores Humanos; 
- Lingüística; 
- Inteligência Artificial; 
- Filosofia, Sociologia e Antropologia; 
- Engenharia e Design.

#### As quatro principais preocupações de IHC

- Ergonomia.
- Usabilidade.
- Acessibilidade.
- Comunicabilidade.

##### Ergonomia

- É a ciência que se preocupa com a relação homem-trabalho e respectivos problemas relativos à saúde e segurança no trabalho.
- Exemplos comuns incluem: cadeiras projetadas para evitar que os usuários sentem em posições que tenham um efeito nocivo na coluna vertebral e a escrivaninha ergonômica que oferece uma gaveta ajustável para o teclado, uma tampa principal com altura ajustável e outros elementos que podem ser modificados pelo usuário. 

###### Ergonomia na TI

Estudos referentes ao conforto, utilização, organização e documentação do software objetivando facilitar e aperfeiçoar o trabalho do usuário junto ao computador.

###### Ergonomia Física

- Está relacionada às características da anatomia humana, antropometria, fisiologia e biomecânica em sua relação à atividade física.

- Os tópicos relevantes incluem o [estudo](http://informatica.hsw.uol.com.br/nervo.htm) da postura no trabalho, manuseio de materiais, movimentos repetitivos, distúrbios músculos-esqueléticos relacionados ao trabalho, projeto de posto de trabalho, segurança e saúde.

###### Ergonomia Cognitiva

- Refere-se aos processos mentais, tais como percepção, memória, raciocínio e resposta motora conforme afetem as interações entre seres humanos e outros elementos de um sistema. 

- Os tópicos relevantes incluem o estudo da carga mental de trabalho, tomada de decisão, [desempenho](http://informatica.hsw.uol.com.br/trabalho-second-life.htm) especializado, interação homem computador, stress e treinamento conforme esses se relacionem a projetos envolvendo seres humanos e sistemas.

###### Ergonomia Cognitiva
- Esta porção psicológica da ergonomia é usualmente referida como Fatores Humanos ou Engenharia de Fatores Humanos nos Estados Unidos da América, enquanto Ergonomia cognitiva é o termo usado na Europa. 
- O entendimento do projeto em termos de carga de trabalho mental, erro humano, a maneira como seres humanos percebem o ambiente que os cerca e, muito importante, as tarefas que eles executam, são todos analisados pelos ergonomistas.

###### Critérios ergonômicos (Scapin e Bastien, 1997)

(@) Guidagem.

&emsp; Refere-se aos meios disponíveis para informar, instruir e orientar os usuários ao longo de suas interações com um computador (mensagens, alarmes, rótulos, etc), inclusive do ponto de vista lexical:

: - Incitação.
: - Agrupamento de items.
: - Feedback imediato.
: - Legibilidade.


(@) Carga de trabalho.

&emsp; Diz respeito a todos elementos da interface que desempenham um papel na redução da carga perceptual ou cognitiva dos usuários, e no aumento da eficiência do diálogo:

: - Brevidade (Concisão e Ações Minímas).
: - Densidade da Informação.


(@) Controle Explícito.

&emsp; Refere-se ao sistema processar as ações solicitadas
pelo usuário e permitir ao usuário o controle explícito
das suas ações sobre o sistema:

: - Ação explícita do usuário.
: - Controle do usuário.

(@) Adaptabilidade.

&emsp; Capacidade do sistema de se comportar contextualmente de acordo com as necessidades dos usuários e preferências:

: - Flexibilidade.
: - Experiência do usuário.

(@) Gerência de Erros.

&emsp; Refere-se à capacidade de prevenir ou evitar erros e recuperar-se dos mesmos quando estes ocorrerem:

: - Prevenção de erros.
: - Qualidade das mensagens de erros.
: - Correção de erros.

(@) Consistência.

&emsp; Refere-se a capacidade das escolhas do projeto de interface (códigos, formatos etc) serem mantidas iguais contextos similares e diferentes em contextos diferentes.

(@) Significância de códigos.

&emsp; Refere-se à utilização de códigos/siglas que sejam familiares ou que tenham explicações na interface.

(@) Compatibilidade.

&emsp; Refere-se à combinação entre características do usuário em relação à tarefa a ser executada e a organização das E/S de uma aplicação.

###### Regras para boa ergonomia (Borges, sd)

- Esforço Mínimo do Usuário: visa reduzir o número de ações a serem desempenhadas pelo usuário, sendo delegadas tais atividades ao sistema, evitando falhas;
- Memória Mínima do Usuário: busca uma curva de aprendizado acelerado, de forma que o uso do software possa ser aprendido facilmente e com o mínimo de informações a serem memorizadas;
- Frustração Mínima: o sistema deve facilitar a execução das tarefas do usuário, removendo possíveis empecilhos e oferecendo a informação necessária para que toda ação seja bem sucedida;
- Maximizar o uso de padrões e hábitos: busca facilitar o aprendizado, reduzir a quantidade de informações a serem memorizadas e minimizar o esforço do usuário;
- Máxima tolerância para diferenças humanas: o sistema deve adequar-se aos diferentes perfis de usuários;
- Máxima tolerância para mudanças ambientais: o sistema deve suportar mudanças do ambiente de hardware/software com um mínimo de esforço do usuário;
- Notificação imediata de problemas: capacidade do sistema de notificar o usuário sobre problemas atuais ou futuros de forma clara e coerente;
- Controle máximo de tarefas pelo usuário: sempre que possível, o usuário deve ter total controle sobre a seqüência de execução das tarefas;
- Apoio máximo às tarefas: o sistema deve ser auto-suficiente quanto à capacidade de oferecer as informações necessárias para a conclusão de uma tarefa.

###### Ergonomia Organizacional

- Concerne à otimização dos sistemas sócio-técnicos, incluindo suas estruturas organizacionais, políticas e de processos. 
- Os tópicos relevantes incluem comunicações, gerenciamento de projetos de trabalho, organização temporal do trabalho, trabalho em grupo, projeto participativo, novos paradigmas do trabalho, trabalho cooperativo, cultura organizacional, organizações em rede, tele-trabalho e gestão da qualidade.

##### Usabilidade

É a extensão na qual um produto pode ser usado por usuários específicos para alcançar objetivos específicos com efetividade, eficiência e satisfação em um contexto de uso específico (ISO).

##### Acessibilidade

Possibilidade de qualquer pessoa (ou o maior número de pessoas) utilizar o sistema e beneficiar-se de suas funcionalidades;

##### Comunicabilidade
Qualidade que determina se as intenções de design de um software são comunicadas adequadamente aos seus usuários.

### Enfoque do desenvolvimento de software com relação ao IHC

Sistema: 
- Qual é a melhor arquitetura para o software? 
- Que padrões de projeto utilizar? 
- Qual linguagem de programação utilizar? 
- Que funcionalidades o sistema deverá implementar? 
- Quais requisitos implementar? 

Usuário: 
- Como as requisitos do usuário podem ser implementados de forma a minimizar seu esforço e facilitar o uso? 
- Para facilitar o aprendizado e uso, as funcionalidades devem ser agrupadas em relação a que classificação?

#### Contexto de uso

Contexto é qualquer informação que pode ser utilizada para caracterizar a situação de uma entidade. 

Uma entidade é uma pessoa, lugar ou objeto que é considerado relevante para a interação entre um usuário e uma aplicação, incluindo o usuário e a própria aplicação. 
- (Abowd et al., 1999).

Contexto de uso de um sistema interativo é definido por três classes de entidades:
- O **usuário** para o qual o sistema se destina e que efetivamente usa o sistema.
- As **plataformas** de software e hardware utilizadas para interação com o sistema.
- O **ambiente físico** onde a interação acontece na prática.

### Contexto

Aspectos importantes para o contexto (Schilit et al., 1994):
- Onde você está ?
- Com quem você está?
- Quais recursos estão próximos de vocês?
- Como está sendo realizada a interação?

### Interface

Principais tipos de interface: 
- Textuais; 
- Gráficas; 
- Sonoras; 

### Interação

Processo de comunicação entre usuário e sistema que se dá por meio da interface. 

Principais tipos de interação: 
* Em linguagem por comando (Command Language); 
* Com diálogo guiado por pergunta e resposta; 
* Com seleção por menu; 
* Comando por voz; 
* WYSIWYG; 
  + What You See Is What You Get 
  + O que você vê é o que você tem
* Manipulação Direta; 
* Gestos

#### Em linguagem por Comando (Command Language) 

* Interação por meio de um conjunto de vocábulos; 
* Regras rígidas de sintaxe; 
* Exemplo: DOS;
* Pontos **positivos** e **negativos**: 
  + Maior potencial e flexibilidade; 
  + Exige bastante treinamento e memória; 

#### Com diálogo criado por perguntas e respostas

* Interação determinada por uma sequência
* Exemplos: DOSVox, Assistentes, Formulários 
* Pontos **positivos** e **negativos**: 
  + Facilidade de uso; 
  + Pouca flexibilidade;

#### Opções acessíveis via menu

* Exemplo: Windows 1.0 
* Pontos **positivos** e **negativos**: 
  - Rapidez do aprendizado; 
  - Necessidade de vários níveis de menu; 

#### Por comando de voz

* O computador responde a comandos do usuário; 
* Exemplos: ViaVoice ou Dragon Naturally Speaking
* Pontos **positivos** e **negativos**: 
  + Acessível para usuários deficientes; 
  + Rápido acesso à conteúdos; 
  + O computador deve ser treinado (específico para um usuário); 
  + Não é 100% precisa; 
  + Não permite realizar todas as tarefas; 

#### What you see is what you get (WYSIWYG)

* A imagem de manipulação da interface é a mesma que a aplicação cria. 
* Ex: Editores de texto, como o Word da Microsoft. 
* “Aqui você tem **negrito**, <u>sublinhado</u> e *itálico*
* Pontos **positivos** e **negativos**: 
  + O usuário vê na tela aquilo que terá na impressão; clareza. 
  + Existem aplicações que não conseguem ser implementadas neste estilo (estilo restrito);

#### Manipulação Direta

* Todos os objetos, atributos e relações são expressos e operados visualmente; 
* As operações são realizadas por ações sobre a representação na tela. 
* Exemplo: para excluir um arquivo, ele sofre “dragging” e é levado para a lixeira; 
* Metáfora.
* Pontos **positivos** e **negativos**: 
  + Uso fácil por usuários leigos; 
  + Aprendizado rápido; 
  + Lenta para os usuários experientes; 
  + Buscas difíceis sobre conjuntos extensos de elementos. Exemplo: exclusão de arquivo: excluir *.txt é mais fácil;

#### Gestos

* Interface controlada por gestos; 
* Exemplo: XBOX Kinetics; 
* Pontos **positivos** e **negativos**: 
  + Uso mais natural; 
  + Pode proporcionar um interação mais agradável; 
  + Uso por deficientes físicos; 
  + Ainda não está tão bem desenvolvido e, por isso, possui limitações;

#### Outros paradigmas de interação 

- Computação ubíqua (“onipresente”).
- Computação pervasiva (“difundida por toda parte”).
- Computação vestível.
- Interação tangível.
- Ambientes atentos.
- Interação cérebro computado.

#### Computação Ubíqua

- Os computadores desapareceriam no ambiente, não mas os perceberíamos e acabaríamos por utilizá-los sem sequer pensar neles.
- No momento atual com os sistemas baseados em multimidia e realidade virtual somos forçados a concentrar nossa atenção nos botões, menus, ou nos movermos em um mundo virtual simulado.
- Como as tecnologias podem ser projetadas a desaparecer do cenário? Como será a IHC?

#### Computação Vestível

- Embarque da tecnologia nas roupas
- Joias, bonés, óculos já foram testados visando a fornecer meios para o usuário interagir com informações digitais enquanto se movimento no mundo fisico.

#### Interação tangível, realidade aumentada e integração física e virtual

- Significa capaz de ser tocado ou pego e percebido pelo sentido do tato. 
- Em outras palavras se quiséssemos usar uma ferramenta virtual na tela-digamos uma caneta- usaríamos uma caneta real, física, que em algum sentido fosse mapeada no equivalente virtual.

#### Ambientes atentos

- O computador atende as necessidades do usuário antecipando o que desejam fazer . A tarefa de decidir o que fazer é repassada ao computador.
- As interfaces respondem a expressões e gestos dos usuários. 

#### Interface cérebro computador

- BCI da sigla em inglês para Brain Computer Interface

##### Fase de pesquisa 

- A Neural Signal está desenvolvendo uma tecnologia para restabelecer a fala de pessoas deficientes. Um implante numa área do cérebro associada com a fala (área de Broca) transmitiria sinais a um computador e, depois, a um microfone. Com treinamento, a pessoa pode aprender a pensar em cada um dos 39 fonemas do idioma inglês e reconstruir a fala através do computador e do microfone [fonte: Neural Signals].
- A [Nasa](http://informatica.hsw.uol.com.br/nasa.htm) tem pesquisado um sistema similar que lê os sinais [elétricos](http://informatica.hsw.uol.com.br/eletricidade.htm) dos [nervos](http://informatica.hsw.uol.com.br/nervo.htm) na área da boca e da garganta, em vez de fazê-lo diretamente a partir do cérebro. Eles tiveram sucesso ao realizarem uma busca "digitando" mentalmente a palavra "Nasa" no Google [fonte: New Scientist].
- A Cyberkinetics Neurotechnology Systems está comercializando o BrainGate, um sistema de interface neural que permite que pessoas com deficiências controlem cadeiras de rodas, próteses mecânicas ou cursores de computador [fonte: Cyberkinetics].
- Pesquisadores japoneses desenvolveram uma BCI preliminar que permite que o usuário controle seu avatar no mundo virtual [Second Life](http://informatica.hsw.uol.com.br/trabalho-second-life.htm) [fonte: Ars Technica].


## Aula 03: 

# Anotações de Leitura

## Livro ?

### Capítulo 1: 


# Tarefas

- [Estudar ???]
